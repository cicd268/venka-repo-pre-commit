# CDP Pre-commit libraries

The project is intended to be a repo of all the cdp related pre-commit modules.

## Version convention 

All the generated artifacts from these projects contain, as part of their names, a version. 

The following convention is recommended to set a valid version that can be used to: 

Be compliant with Python PEP rules and Python distributions 

https://the-hitchhikers-guide-to-packaging.readthedocs.io/en/latest/specification.html 

https://www.python.org/dev/peps/pep-0440/ 

Create different types of releases in an easy way (release candidates, hotfixes, final releases, etc) 

The version would be as followed: 

**major.minor.patch** 

For example: 

**1.0.2** 

The following criteria should be followed when using the previous convention: 

Use the last number (patch) to create RCs (release candidates) and Hotfixes 

Use the major and minor numbers to set an initial version that may evolve into different patches (RCs and other fixes) 

The following example can be applied: 

Start a new release with the version 0.1.0 

Create the first release candidate with version 0.1.0 

If a bug is found during QA, fix and create a new version 0.1.1. This is the second release candidate 

Deploy 0.1.1 into production 

If a bug is found in production, fix it and generate a new version 0.1.2. This is a hotfix 

The next release for the same project will be 0.2.0

## Configuration files

***module_name*/setup.py ** 

setup.py contains the config info as well. This is a workaround for the limitation of unable 
to load pyyaml in new venv needed by pre-commit

The following properties can be found: 

* **Version** 

  The artifacts version. Use recommended convention here 

* **Artfact name** 

  The name that will be used to create the artifacts, together with the previous version 

* **Author** 

  Author name 

* **Author email** 

  Author email address 

* **Description** 

  Short description of the project 

* **Url** 

  Gitlab project URL 

All the previous details are mandatory. The below ones are optional: 

* **run_test** 

  If this variable is present, unit tests will be run 

  It is recommended to have and run unit tests, but in case no tests are available, this step can be skipped by removing or commenting out this variable. *DO NOT SET IT TO FALSE, INSTEAD REMOVE OR COMMENT OUT THE VARIABLE TO SKIP TESTS*


**README.md** 

This is a file in Markdown syntax that will be used as the description of the current project when generating the Python wheel distributions. Additional information on Markdown syntax can be found here: 

https://www.markdownguide.org/basic-syntax/ 

**Requirements.txt (optional)** 

Add the Python requirements to this text file. Include the Python package name and version. This file will be used to obtain the dependencies for the code that can be deployed separately into the Lambda function as a Lambda layer. 

**Requirements_test.txt (optional)**

Add any additional requirement that may be different from the previous for unit tests. These requirements will be used to run unit tests only. Only requirements that ARE NOT INCLUDED IN THE PREVIOUS have to be included here. 

If requirements.txt does not exist or it is empty, the requirements zip file won’t be generated 

Release versions cannot be overridden. The pipeline will break and show an error message if the release version already exists. To fix this, create a new version by updating the version value in the *config.yml*.

**version.py**

Set the version for the generated artifact.

## Gitlab pipeline 

The Gitlab pipeline used for this kind of projects contain the following steps: 

* **Clone repository** 

  Git clone the repository with the code from the selected branch 

* **Fetch logic** 

  Git clone the pipeline logic that will be used to run the rest of the pipeline 

* **Validate pipeline** 

  Check that the pipeline can be triggered based on the selected branch strategy, selected branch, etc 

* **Configuration setup** 

  Add configuration files if required (setup.py, tox.ini, etc) 

* **Linting** 

  Run code styles checks using Flake8 

* **Unit tests** 

  Run unit tests with pytest if required 

* **Distribution** 

  Generate artifacts for further distribution (Zip files, wheel files, etc) 
  Upload artifacts to S3 bucket 

  Upload wheel files to external Bayer Artifactory so it can be used as Lambda and Glue job dependencies 

* **Deployment** 

  If required, deploy code to AWS Lambda in selected environment (DEV, TEST) 

**Trigger** 

The pipeline automatically triggers after pushing any change. The change has to be inside one of the existing modules (Lambda function folders).

## Usage

**Install pre-commit in the dev environment*
```
pip install pre-commit
```
This will create a folder ~/.cache/pre-commit under HOME directory.
Inside this folder, there will be git repo downloaded from 
the pre-commit repo. This is FYI - do not modify files here.

**Add the .pre-commit-config.yaml to your git root directory*
- Example is provided below.
```
  - repo: https://gitlab.bayer.com/ph-cdp/commons/libraries/cdp-pre-commit.git
    rev: v0.1
    hooks:
      - id: check-python-lib-pinned-ver
        name: Check whether python libraries use a pinned version
        entry: check-python-lib-pinned-ver
        args: [ --run=all, --output=error.log ]
        language: python
        pass_filenames: false
```

**Add pre-commit file*
- Place the below contents in the file pre-commit under .git/hook folder
of your project directory

```
#!/bin/sh
#!/usr/bin/env bash
# File generated by pre-commit: https://pre-commit.com
# ID: 138fd403232d2ddd5efb44317e38bf03

# start templated
INSTALL_PYTHON='<VIRTUAL_ENV_DIRECTORY_PREFIX>>\venv\Scripts\python.exe'
ARGS=(hook-impl --config=.pre-commit-config.yaml --hook-type=pre-commit)
# end templated

HERE="$(cd "$(dirname "$0")" && pwd)"
ARGS+=(--hook-dir "$HERE" -- "$@")

if [ -x "$INSTALL_PYTHON" ]; then
    exec "$INSTALL_PYTHON" -mpre_commit "${ARGS[@]}"
elif command -v pre-commit > /dev/null; then
    exec pre-commit "${ARGS[@]}"
else
    echo '`pre-commit` not found.  Did you forget to activate your virtualenv?' 1>&2
    exit 1
fi
```

**Run the pre-commit for all files*
- Run your normal git commit command and the pre-commit in .pre-commit-config.yaml
will get fired.
```
git commit -m "<comment>"
```

**Run the pre-commit for all files (irrespective of the file status)*
```
pre-commit run --all-files
```
pre-commit in .pre-commit-config.yaml will get fired.

